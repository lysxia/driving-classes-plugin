# Deriving without spelling out "deriving"

This GHC plugin automatically adds deriving clauses to all data types in
a module.

## Examples

The following module uses this plugin to derive `Eq`, `Ord`, `Show` for all
types by default, excluding invalid instances with the `NoDriving` option.

```haskell
{-# LANGUAGE DerivingStrategies #-}
{-# OPTIONS_GHC -fplugin=Driving.Classes #-}

module X where

{-# ANN module (Driving :: Driving
  '[ Stock '(Eq, Ord, Show)
   , NoDriving '(Eq MyEndo, Ord MyEndo, Show MyEndo)
   ]) #-}

data T = C1 | C2
data U = D1 | D2
data V = E1 | E2

newtype MyEndo a = MyEndo (a -> a)
```

Deriving `Functor` and `Applicative` via `WrappedMonad`:

```haskell
{-# LANGUAGE DerivingStrategies, GeneralizedNewtypeDeriving, DerivingVia #-}
{-# OPTIONS_GHC -fplugin=Driving.Classes #-}

module X where

import Control.Applicative (WrappedMonad(..))

{-# ANN module (Driving :: Driving
  '[ '(Functor, Applicative) `ViaF` WrappedMonad
   , Newtype Monad
   ]) #-}

newtype A a = A [a]
newtype B a = B (IO a)
newtype C a = C (Maybe a)
```
