# Revision history for driving-classes-plugin

Latest version: https://gitlab.com/Lysxia/driving-classes-plugin/-/blob/main/CHANGELOG.md

## 0.1.4.0

- Compatibility with GHC 9.4

## 0.1.3.0

- Compatibility with GHC 9.2

## 0.1.2.0

- Compatibility with GHC 8.6 and 8.8
- Fix documentation

## 0.1.1.0

- Compatibility with GHC 8.10

## 0.1.0.0

* Create driving-classes-plugin
